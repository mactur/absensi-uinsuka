//
//  DashboardViewController.swift
//  UINSunanKalijaga
//
//  Created by MacBook on 10/27/16.
//  Copyright © 2016 Joskoding. All rights reserved.
//

import UIKit
import CoreLocation
class DashboardViewController: UIViewController, CLLocationManagerDelegate {
    let defaults = UserDefaults.standard
    let locationMgr = CLLocationManager()
    @IBOutlet weak var fullnameLabel: UILabel!
    @IBOutlet weak var nipLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        fullnameLabel.text = defaults.string(forKey: "fullname")
        nipLabel.text = defaults.string(forKey: "nip")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func attendButton(_ sender: AnyObject) {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined{
            locationMgr.requestWhenInUseAuthorization()
            return
        }
        
        if status == .denied || status == .restricted{
            let alert = UIAlertController(title: "GPS Tidak aktif", message: "UIN Attend Tidak Berjalan, Aktifkan ?", preferredStyle: UIAlertControllerStyle.alert)
            let oke = UIAlertAction(title: "Oke", style: UIAlertActionStyle.default, handler: nil)
            alert.addAction(oke)
            
            present(alert, animated: true, completion: nil)
            return
        }
        locationMgr.delegate = self
        locationMgr.startUpdatingLocation()
        self.performSegue(withIdentifier: "showattend", sender: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latitude = locations.last?.coordinate.latitude
        let longitude = locations.last?.coordinate.longitude
        self.defaults.set(latitude, forKey: "latitude")
        self.defaults.set(longitude, forKey: "longitude")
        self.defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Terjadi Kesalahan")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
